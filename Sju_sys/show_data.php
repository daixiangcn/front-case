<!DOCTYPE html>
<html class="x-admin-sm">
<?php
session_start();
header("P3P: CP=CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR");
if (empty($_SESSION['uid'])) {
    echo "提示：您还没有登录，不能访问当前页面！<a href='login.php'>前往登录页面</a>";
    exit;
}
?>
<head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.2</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php
$random = $_GET["random"];
$course = $_GET["course"];
$college = $_GET["college"];
?>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <!--                <div class="layui-card-header">-->
                <!--                    未学习学生数据-->
                <!--                </div>-->
                <div class="layui-card-body ">
                    <form class="layui-form layui-col-space5">
                        <div class="layui-inline layui-show-xs-block">
                            <input type="text" name="userName" id="userName" placeholder="支持姓名/学号/班级查找"
                                   autocomplete="off"
                                   required="required"
                                   class="layui-input"></div>
                        <div class="layui-inline layui-show-xs-block">
                            <button class="layui-btn" lay-submit="" lay-filter="sreach" data-type="reload"
                                    onclick="return false;"
                                    id="selectbyCondition">
                                <i class="layui-icon">&#xe615;</i></button>
                        </div>
                    </form>
                </div>
                <div class="layui-card-body ">
                    <table id="demo" class="layui-hide" lay-filter="demo"></table>
                    <div id="pageUD"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/html" id="toolbarDemo">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-event="getCheckData"> 获取选中行数据</button>
        <button class="layui-btn layui-btn-sm" lay-event="getCheckLength">获取选中数目</button>
        <button class="layui-btn layui-btn-sm" lay-event="isAll"> 验证是否全选</button>
    </div>
</script>
<script src="js/jquery.min.js"></script>
<script>
    var limit = 10;
    var page = 1;
    var searchName = "";
    layui.use('table', function () {
        var table = layui.table;
        table.render({
            elem: '#demo',
            method: 'post',
            url: 'paging.php?random=' + '<?php echo $random?>',
            limit: limit,
            page: page,
            id: 'userTableReload',
            title: '<?php echo $college?>' + '<?php echo $course?>' + searchName + '未学习学生数据记录表',
            toolbar: '#toolbarDemo',
            cellMinWidth: 50, //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            cols: [[
                {checkbox: true},
                {field: 'number', sort: true, title: '学号'},
                {field: 'name', sort: true, title: '姓名'},
                {field: 'gender', sort: true, title: '性别'},
                {field: 'major', sort: true, title: '专业'},
                {field: 'college', sort: true, title: '学院'},
                {field: 'grade', sort: true, title: '年级'},
                {field: 'class', title: '班级'}
            ]],
            done: function (res) {
                //如果是异步请求数据方式，res即为你接口返回的信息。
                // console.log("done");
                console.log(res);
            }
        });
        //点击搜索按钮根据用户名称查询
        $('#selectbyCondition').on('click',
            function () {
                // if ($('#userName').val() == '') {
                //     layui.use('layer', function () {
                //         var layer = layui.layer;
                //         layer.open({
                //             title: '提示消息',
                //             content: '查询条件不能为空！'
                //         });
                //
                //     });
                // } else {
                //根据条件查询表格数据重新加载
                searchName = $('#userName').val();
                table.reload('userTableReload', {
                    url: 'search.php?random=' + '<?php echo $random?>',
                    where: { //设定异步数据接口的额外参数，任意设
                        userName: $('#userName').val()
                    }
                    ,
                    page: {
                        curr: 1 //重新从第 1 页开始
                    },
                    title: '<?php echo $college?>' + '<?php echo $course?>' + searchName + '未学习学生数据记录表'+<?php echo '"'.strval(date('Y-m-d H:i:s', time())).'"'?>,
                });
                // }
            });
        //头工具栏事件
        table.on('toolbar(demo)',
            function (obj) {
                var checkStatus = table.checkStatus(obj.config.id);
                switch (obj.event) {
                    case 'getCheckData':
                        var data = checkStatus.data;
                        layer.alert(JSON.stringify(data));
                        break;
                    case 'getCheckLength':
                        var data = checkStatus.data;
                        layer.msg('选中了：' + data.length + ' 个');
                        break;
                    case 'isAll':
                        layer.msg(checkStatus.isAll ? '全选' : '未全选');
                        break;
                }
            }
        );
    });

    layui.use('laydate', function () {
        var laydate = layui.laydate;
        //执行一个laydate实例
        laydate.render({
            elem: '#start' //指定元素
        });
        //执行一个laydate实例
        laydate.render({
            elem: '#end' //指定元素
        });
    });
</script>
</body>
</html>