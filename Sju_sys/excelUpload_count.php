<!DOCTYPE html>
<html class="x-admin-sm">
<?php
session_start();
header("P3P: CP=CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR");
if (empty($_SESSION['uid'])) {
    echo "提示：您还没有登录，不能访问当前页面！<a href='login.php'>前往登录页面</a>";
    exit;
}
?>
<head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.2</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <script src="js/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php
$college = "";
?>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">
                    数据上传结果
                </div>
                <div class="layui-card-body ">
                    <?php
                    require('library/php-excel-reader/excel_reader2.php');
                    require('library/SpreadsheetReader.php');
                    require('db_config.php');
                    $random = time();  // 每次查询生成一个随机数
                    if (isset($_POST['Submit'])) {
                        $mimes = ['application/vnd.ms-excel','application/octet-stream', 'text/xls', 'application/vnd.oasis.opendocument.spreadsheet'];
//                        print_r($_FILES);
                        if (in_array($_FILES["file"]["type"], $mimes)) {
                            $uploadFilePath = 'files/upload/' . time() . basename($_FILES['file']['name']);
                            move_uploaded_file($_FILES['file']['tmp_name'], $uploadFilePath);
                            $Reader = new SpreadsheetReader($uploadFilePath);
                            // $totalSheet = count($Reader->sheets());
                            // echo "你有 ".$totalSheet." 张表".
                            $html = "<blockquote class=\"layui-elem-quote\">提示信息：已成功添加Excel中的数据至数据库,此次上传的数据如下表所示。点击按钮查询此次未学习名单 <button type=\"button\" class=\"layui-btn \" ><a id='mingdan' style='color: #fff'>查看此次未学习名单</a></button></blockquote><table class='layui-table' style='text-align: center'><thead><tr><th style='text-align: center'>学生姓名</th><th style='text-align: center'>所属院系</th><th style='text-align: center'>课程名称</th></tr></thead><tbody>";
                            // for($i=0;$i<$totalSheet;$i++){
                            // $Reader->ChangeSheet($i);
                            $Reader->ChangeSheet(0);
                            $i = 0;
                            $str = "";
                            foreach ($Reader as $Row) {
                                $html .= "<tr>";
                                $name = isset($Row[0]) ? $Row[0] : '';
                                $college = isset($Row[2]) ? $Row[2] : '';
                                $class = isset($Row[3]) ? $Row[3] : '';
                                if ($i == 0) {

                                } else {
                                    $html .= "<td>" . $name . "</td>";
                                    $html .= "<td>" . $college . "</td>";
                                    $html .= "<td>" . $class . "</td>";
                                    $html .= "</tr>";
                                    $str = $str . "('" . $name . "','" . $college . "','" . $class . "'),";
                                }
                                $i++;
                            }
                            $str = substr($str, 0, strlen($str) - 1);
                            $html .= "</tbody></table>";
                            echo $html;
                            $sql = "CREATE TABLE `" . $random . "_temp`  (`name` varchar(50) ,`college` varchar(50),`class` varchar(50))";
                            $mysqli->query($sql);
//                            echo $sql;
                            $sql = "CREATE TABLE `" . $random . "`  (`number` varchar(25),`name` varchar(25),`gender` varchar(5),`grade` varchar(25),`class` varchar(25),`college` varchar(25),`major` varchar(25),`campus` varchar(15))";
                            $mysqli->query($sql);
//                            echo $sql;
                            $sql = "insert into " . $random . "_temp(name,college,class) values" . $str;
                            $mysqli->query($sql);
                            $sql = "update `" . $random . "_temp` set name = replace(name,' ','') ";
                            $mysqli->query($sql);
                            $sql = "update `" . $random . "_temp` set name = replace(name,',','') ";
                            $mysqli->query($sql);
                            $sql = "update `" . $random . "_temp` set name = replace(name,'，','') ";
                            $mysqli->query($sql);
                            $sql = "select college from `" . $random . "_temp` limit 1";
                            $result = $mysqli->query($sql);
                            $college = trim(mysqli_fetch_row($result)[0]);
                            $sql = "select class from `" . $random . "_temp` limit 1";
                            $result = $mysqli->query($sql);
                            $class = trim(mysqli_fetch_row($result)[0]);
//                            echo $college;
                            echo "<script>$(function() {
                              $('#mingdan').attr('onclick',\"xadmin.open('$college $class 未参与学习名单','show_data.php?random=" . $random . "&class=" . $class . "&college=" . $college . "')\" );
                            })</script>";
                            $sql = "insert into `" . $random . "` (select * from " . $college . " where (name not in ( select name from " . $college . " where (" . $college . ".name in (select name from `" . $random . "_temp`) or number in (select name from `" . $random . "_temp`) or concat(number," . $college . ".name) in (select name from `" . $random . "_temp`) or concat(" . $college . ".name,number) in (select name from `" . $random . "_temp`) or concat(class," . $college . ".name) in (select name from `" . $random . "_temp`) or concat(" . $college . ".name,class) in (select name from `" . $random . "_temp`) or concat(" . $college . ".name,number,class) in (select name from `" . $random . "_temp`)  or concat(" . $college . ".name,class,number) in (select name from `" . $random . "_temp`) or concat(number,class," . $college . ".name) in (select name from `" . $random . "_temp`) or concat(number," . $college . ".name,class) in (select name from `" . $random . "_temp`) or concat(class,number," . $college . ".name) in (select name from `" . $random . "_temp` ) or concat(class," . $college . ".name,number) in (select name from `" . $random . "_temp` )))))";
                            $mysqli->query($sql);
//                            echo $sql;
                            $sql = "DROP TABLE IF EXISTS `" . $random . "_temp`";
                            $mysqli->query($sql);
                            $sql = "update sys_data set file = file + 1,search = search + 1";
                            $mysqli->query($sql);
                        } else {
                            die("<script>layui.use('layer', function () {var layer = layui.layer;layer.open({type: 0, title: '提示信息', content: '对不起，您上传的不是符合要求的文件，仅允许上传后缀名为.xls的Excel文件,请将文件在Excel中另存为.xls文件！'});});</script>");
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>