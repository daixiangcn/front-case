<!DOCTYPE html>
<?php
session_start();
header("P3P: CP=CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR");
if (empty($_SESSION['uid'])) {
    echo "提示：您还没有登录，不能访问当前页面！<a href='login.php'>前往登录页面</a>";
    exit;
}
?>
<html class="x-admin-sm">
<head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.2</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div style="padding: 10px; background-color: #F2F2F2;">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md6">
            <div class="layui-card">
                <div class="layui-card-header">使用说明</div>
                <div class="layui-field-box">
                    <ul class="layui-timeline">
                        <li class="layui-timeline-item">
                            <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                            <div class="layui-timeline-content layui-text">
                                <p>步骤一：获取Excel</p>
                                <ul>
                                    <li>从“青年大学习”官方平台导出Excel数据表格;</li>
                                    <li>查看Excel表格的前4列的列标题是否如【参考样表】所示，如不一致，需要调整一致。</li>
                                    <li>需要注意的是：文件后缀必须是“xls”</li>
                                </ul>
                            </div>
                        </li>
                        <li class="layui-timeline-item">
                            <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                            <div class="layui-timeline-content layui-text">
                                <p>步骤二：上传Excel</p>
                                <ul>
                                    <li>点击下方上传按钮，选择准备好的文件，点击【立即提交】；</li>
                                    <li>如提示[文件类型不对]，则需要将文件在Excel软件中另存为xls后缀的文件；</li>
                                    <li>如您在文件上传过程遇到问题，请联系技术支持工程师协助解决。</li>
                                </ul>
                            </div>
                        </li>
                        <li class="layui-timeline-item">
                            <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                            <div class="layui-timeline-content layui-text">
                                <p>步骤三：查询数据</p>
                                <ul>
                                    <li>【未参与学习名单】默认显示的数据为上传的Excel数据和全院匹配的结果；</li>
                                    <li>在输入框输入班级号，可按班级查询，全选后可导出Excel；不输入内容，直接点击查询按钮，可列出全院信息，全选可以导出全院信息。</li>
                                    <li>在输入框输入姓名，可查询指定学生，无数据表示该学生已参与学习；</li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="layui-col-md6">
            <div class="layui-card">
                <div class="layui-card-header">参考样表</div>
                <div class="layui-field-box">
                    <table class="layui-table">
                        <thead>
                        <tr>
                            <th>姓名</th>
                            <th>报名时间</th>
                            <th>所在组织</th>
                            <th>报名课程</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>32017061006张三</td>
                            <td>2020-02-24 11:55:18</td>
                            <td>计算机科学与工程学院</td>
                            <td>第八季第二期</td>
                        </tr>
                        </tbody>
                    </table>
                    <p>注意：学生在“青年大学习”填写信息时需要<span style="color: red;font-weight: bolder">填写完整的班级号+姓名或者完整的学号+姓名</span>，如：32017051A张三，32017051033李四。<span style="color: red;font-weight: bolder">学号或班级号填写不完整的学生会被系统识别为“未完成学习”</span>。</p>
                </div>
            </div>
        </div>

        <div class="layui-col-md6">
            <div class="layui-card">
                <div class="layui-card-header">视频教程</div>
                <video controls="controls" width="100%" poster="videos/001.png" oncontextmenu="return false">
                    <source src="videos/001.mp4" type="video/mp4">
                </video>
            </div>
        </div>

        <div class="layui-col-md6" style="margin-top: -70px;">
            <div class="layui-card">
                <div class="layui-card-header">文件上传</div>
                <form action="excelUpload.php" enctype="multipart/form-data" method='post' class="layui-form">
                    <div class="layui-form-item">
                        <label class="layui-form-label">选择文件：</label>
                        <div class="layui-input-block">
                            <input type="file" name="file" accept=".xls" class="">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" lay-submit lay-filter="formDemo" type="submit" name="Submit"
                                    id="submit">
                                立即提交
                            </button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script>
    $('#submit').click(function () {
        layui.use('layer', function () {
            var layer = layui.layer;
            layer.open({
                title: '提示信息'
                , content: '数据正在上传至服务器,上传完成会自动跳转,请耐心等待！'
            });

        });
    })
</script>
</body>
</html>