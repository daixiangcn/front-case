<!DOCTYPE html>
<html class="x-admin-sm">
<?php
session_start();
header("P3P: CP=CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR");
if (empty($_SESSION['uid'])) {
    echo "提示：您还没有登录，不能访问当前页面！<a href='login.php'>前往登录页面</a>";
    exit;
}
?>
<head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.2</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="js/jquery.min.js"></script>
    <!-- 引入 echarts.js -->
    <script src="https://cdn.bootcss.com/echarts/4.6.0/echarts.min.js"></script>
</head>
<body>
<?php
$random = $_GET["random"];
$course = $_GET["course"];
$college = $_GET["college"];
?>
<div class="layui-fluid">
    <div class="layui-col-md12">
        <div class="layui-card">
            <div class="layui-card-header" style="text-align: center">
                <?php echo $college . $course ?> 青年大学习 数据可视化分析
            </div>
        </div>
    </div>
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md4">
            <div class="layui-card">
<!--                <div class="layui-card-header">-->
<!--                    --><?php //echo $college . $course ?><!--总体情况-->
<!--                </div>-->
                <div class="layui-card-body ">
                    <div id="main1" style="width: 100%;height:400px;"></div>
                    <p>截止时间：<?php echo date('Y-m-d H:i:s', time()) ?></p>
                </div>
            </div>
        </div>
        <div class="layui-col-md4">
            <div class="layui-card">
<!--                <div class="layui-card-header">-->
<!--                    --><?php //echo $college . $course ?><!--班级参与率-->
<!--                </div>-->
                <div class="layui-card-body ">
                    <div id="main2" style="width: 100%;height:400px;"></div>
                    <p>截止时间：<?php echo date('Y-m-d H:i:s', time()) ?></p>
                </div>
            </div>
        </div>
        <div class="layui-col-md4">
            <div class="layui-card">
<!--                <div class="layui-card-header">-->
<!--                    --><?php //echo $college . $course ?><!--班级未参与人数占比-->
<!--                </div>-->
                <div class="layui-card-body ">
                    <div id="main3" style="width: 100%;height:400px;"></div>
                    <p>截止时间：<?php echo date('Y-m-d H:i:s', time()) ?></p>
                </div>
            </div>
        </div>
        <div class="layui-col-md12">
            <div class="layui-card">
                <!--                <div class="layui-card-header">-->
                <!--                    --><?php //echo $college . $course ?><!--总体情况-->
                <!--                </div>-->
                <div class="layui-card-body ">
                    <div id="main4" style="width: 100%;height:400px;"></div>
                    <p>截止时间：<?php echo date('Y-m-d H:i:s', time()) ?></p>
                </div>
            </div>
        </div>
    </div>

    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">
                    <?php echo $college . $course ?>参与度数据概览 | 截止时间：<?php echo date('Y-m-d H:i:s', time()) ?>
                </div>
                <div class="layui-card-body ">
                    <form class="layui-form layui-col-space5">
                        <div class="layui-inline layui-show-xs-block">
                            <input type="text" name="userName" id="userName" placeholder="支持班级/专业查找"
                                   autocomplete="off"
                                   required="required"
                                   class="layui-input"></div>
                        <div class="layui-inline layui-show-xs-block">
                            <button class="layui-btn" lay-submit="" lay-filter="sreach" data-type="reload"
                                    onclick="return false;"
                                    id="selectbyCondition">
                                <i class="layui-icon">&#xe615;</i></button>
                        </div>
                    </form>
                </div>
                <div class="layui-card-body ">
                    <table id="demo" class="layui-hide" lay-filter="demo"></table>
                    <div id="pageUD"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/html" id="toolbarDemo">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-event="getCheckData"> 获取选中行数据</button>
        <button class="layui-btn layui-btn-sm" lay-event="getCheckLength">获取选中数目</button>
        <button class="layui-btn layui-btn-sm" lay-event="isAll"> 验证是否全选</button>
        <button type="button" class="layui-btn "><a
                    onclick="xadmin.open(' <?php echo $college . $course ?>未参与学习名单','show_data.php?random=<?php echo $random ?>&course=<?php echo $course ?>&college=<?php echo $college ?>','','',true)"
                    style='color:#fff'>查看详细名单</a></button>
    </div>
</script>
<script>
    // 基于准备好的dom，初始化echarts实例
    var myChart1 = echarts.init(document.getElementById('main1'));
    var myChart2 = echarts.init(document.getElementById('main2'));
    var myChart3 = echarts.init(document.getElementById('main3'));
    var myChart4 = echarts.init(document.getElementById('main4'));
    $.ajax({
        url: 'get_data_pie_class.php?random=' + '<?php echo $random?>',
        dataType: "json",
        success: function (info, status) {
            myChart3.setOption({
                toolbox: { //保存图片
                    feature: {
                        saveAsImage: {}
                    }
                },
                title:{
                    show: true,
                    text:'未参与人数较多的班级',
                    x: 'center',
                    textStyle:{
                        color: '#760002',
                    }
                },
                //提示框组件,鼠标移动上去显示的提示内容
                tooltip: {
                    trigger: 'item',
                    formatter: "{b}: {c}人 ({d}%)"//模板变量有 {a}、{b}、{c}、{d}，分别表示系列名，数据名，数据值，百分比。
                },
                //图例
                // legend: {
                //     //图例垂直排列
                //     orient: 'vertical',
                //     x: 'left',
                //     //data中的名字要与series-data中的列名对应，方可点击操控
                //     data: info.showdata
                // },
                series: [
                    {
                        name: '班级未参与人数前十',
                        type: 'pie',    // 设置图表类型为饼图
                        roseType: 'angle',
                        radius: '60%',  // 饼图的半径，外半径为可视区尺寸（容器高宽中较小一项）的 55% 长度。
                        data: info.showdata,
                        emphasis: {
                            itemStyle: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)',
                                normal: {
                                    label: {
                                        show: true,
                                        formatter: '{b} : {c}人 ({d}%)'
                                    },
                                    labelLine: {show: true}
                                }
                            }
                        }
                    },

                ]
            });
        }
    });
    $.ajax({
        url: 'get_data_pie_all_class.php?random=' + '<?php echo $random?>',
        dataType: "json",
        success: function (info, status) {
            myChart2.setOption({
                toolbox: { //保存图片
                    feature: {
                        saveAsImage: {}
                    }
                },
                title:{
                    show: true,
                    text:'全院班级参与率',
                    x: 'center',
                    textStyle:{
                        color: '#760002',
                    }
                },
                tooltip: {},
                xAxis: {
                    type: 'category',
                    data: info.class,
                },
                yAxis: {
                    type: 'value'
                },
                series: [{
                    name: '班级参与率',
                    type: 'bar',
                    itemStyle: {
                        normal: {
                            color: '#91c7ae',
                        }
                    },
                    data: info.proportion
                }]
            });
            myChart1.setOption({
                toolbox: { //保存图片
                    feature: {
                        saveAsImage: {}
                    }
                },
                title:{
                    show: true,
                    text:'学院总体情况',
                    x: 'center',
                    textStyle:{
                        color: '#760002',
                    }
                },
                //提示框组件,鼠标移动上去显示的提示内容
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"//模板变量有 {a}、{b}、{c}、{d}，分别表示系列名，数据名，数据值，百分比。
                },
                //图例
                legend: {
                    //图例垂直排列
                    orient: 'vertical',
                    x: 'left',
                    //data中的名字要与series-data中的列名对应，方可点击操控
                    data: info.showdata
                },
                series: [
                    {
                        name: '学院总体情况',
                        type: 'pie',    // 设置图表类型为饼图
                        radius: '60%',  // 饼图的半径，外半径为可视区尺寸（容器高宽中较小一项）的 55% 长度。
                        data: info.total,
                        emphasis: {
                            itemStyle: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)',
                                normal: {
                                    label: {
                                        show: true,
                                        formatter: '{b} : {c} ({d}%)'
                                    },
                                    labelLine: {show: true}
                                }
                            }
                        }
                    },

                ]
            });
            myChart4.setOption({
                title: {
                    text: '学生参与学习时间分布图',
                    x: 'center',
                    textStyle:{
                        color: '#760002',
                    }
                },
                tooltip: {  // 悬浮显示信息
                    trigger: 'axis',
                },
                toolbox: { //保存图片
                    feature: {
                        saveAsImage: {}
                    }
                },
                xAxis: {
                    name: '日期时间',
                    show: 'true',
                    type: 'category',
                    boundaryGap: false,
                    data: info.date
                },
                yAxis: {
                    name: '参与人数',
                    show: 'true',
                    type: 'value'
                },
                series: [{
                    data: info.number,
                    type: 'line',
                    smooth: true,
                    areaStyle: {}
                }]
            });
        }
    });
    var limit = 10;
    var page = 1;
    var searchName = "";
    layui.use('table', function () {
        var table = layui.table;
        table.render({
            elem: '#demo',
            method: 'post',
            url: 'paging_class.php?random=' + '<?php echo $random?>',
            limit: limit,
            page: page,
            id: 'userTableReload',
            title: '<?php echo $college?>' + '<?php echo $course?>' + searchName + '未学习学生数据记录表',
            toolbar: '#toolbarDemo',
            cellMinWidth: 80, //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            cols: [[
                {checkbox: true},
                {field: 'college', sort: false, title: '学院'},
                {field: 'major', sort: false, title: '专业'},
                {field: 'class', title: '班级'},
                {field: 'not_involved', sort: true, title: '未参与学习人数'},
                {field: 'total', sort: true, title: '班级总人数'},
                {field: 'proportion', sort: false, title: '参与率'},
            ]],
            done: function (res) {
                //如果是异步请求数据方式，res即为你接口返回的信息。
                // console.log(res);
                // 异步加载数据
            }
        });
        //点击搜索按钮根据用户名称查询
        $('#selectbyCondition').on('click',
            function () {
                // if ($('#userName').val() == '') {
                //     layui.use('layer', function () {
                //         var layer = layui.layer;
                //         layer.open({
                //             title: '提示消息',
                //             content: '查询条件不能为空！'
                //         });
                //
                //     });
                // } else {
                //根据条件查询表格数据重新加载
                searchName = $('#userName').val();
                table.reload('userTableReload', {
                    url: 'search_class.php?random=' + '<?php echo $random?>',
                    where: { //设定异步数据接口的额外参数，任意设
                        userName: $('#userName').val()
                    }
                    ,
                    page: {
                        curr: 1 //重新从第 1 页开始
                    },
                    title: '<?php echo $college?>' + '<?php echo $course?>' + searchName + '班级数据概览' +<?php echo '"' . strval(date('Y-m-d H:i:s', time())) . '"' ?>,
                });
                // }
            });
        //头工具栏事件
        table.on('toolbar(demo)',
            function (obj) {
                var checkStatus = table.checkStatus(obj.config.id);
                switch (obj.event) {
                    case 'getCheckData':
                        var data = checkStatus.data;
                        layer.alert(JSON.stringify(data));
                        break;
                    case 'getCheckLength':
                        var data = checkStatus.data;
                        layer.msg('选中了：' + data.length + ' 个');
                        break;
                    case 'isAll':
                        layer.msg(checkStatus.isAll ? '全选' : '未全选');
                        break;
                }
            }
        );
    });

    layui.use('laydate', function () {
        var laydate = layui.laydate;
        //执行一个laydate实例
        laydate.render({
            elem: '#start' //指定元素
        });
        //执行一个laydate实例
        laydate.render({
            elem: '#end' //指定元素
        });
    });
</script>
</body>
</html>