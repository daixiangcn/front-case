<!doctype html>
<?php
header("Content-type:text/html;charset=utf-8");
session_start();
if (empty($_SESSION['uid'])) {
    echo "提示：您还没有登录，不能访问当前页面！<a href='login.php'>前往登录页面</a>";
    exit;
}

?>
<html class="x-admin-sm">
<head>
    <meta charset="UTF-8">
    <title>共青团三江学院委员会智慧办公系统</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="Shortcut Icon" href="favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <!-- <link rel="stylesheet" href="./css/theme5.css"> -->
    <script src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        // 是否开启刷新记忆tab功能
        // var is_remember = false;
    </script>
</head>
<body class="index">
<!-- 顶部开始 -->
<div class="container">
    <div class="logo">
        <a href="./index.php"><img src="images/tuan_min_logo.png" width="18px"> 团委智慧办公系统</a></div>
    <div class="left_open">
        <a><i title="展开左侧栏" class="iconfont">&#xe699;</i></a>
    </div>
    <ul class="layui-nav right" lay-filter="">
        <li  class="layui-nav-item"><a href="mailto:daixiang@idcs.vip"><i class="iconfont color" > &#xe69f;</i> 联系技术支持工程师</a></li>
        <li class="layui-nav-item">
            <a href="javascript:;"><i class="iconfont color" > &#xe6b8;</i> <? echo $_SESSION['uid'] ?> </a>
            <dl class="layui-nav-child">
                <!-- 二级菜单 -->
                <dd>
                    <a href="./logout.php">退出登录</a></dd>
            </dl>
        </li>
    </ul>
</div>
<!-- 顶部结束 -->
<!-- 中部开始 -->
<!-- 左侧菜单开始 -->
<div class="left-nav">
    <div id="side-nav">
        <ul id="nav">
            <li>
                <a href="javascript:;">
                    <i class="iconfont trade-assurance" lay-tips="青年大学习">&#xe705;</i>
                    <cite>青年大学习</cite>
                    <i class="iconfont nav_right">&#xe697;</i></a>
                <ul class="sub-menu">
                    <li>
                        <a onclick="xadmin.add_tab('导入数据','import_data.php')">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite> 导入数据</cite></a>
                    </li>
                    <li id="copy_style1">
                        <!--                        <a onclick="xadmin.add_tab('统计数据','upload_data.php')">-->
                        <!--                            <i class="iconfont">&#xe6a7;</i>-->
                        <!--                            <cite>统计数据</cite></a>-->
                        <a>
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>统计数据</cite></a>
                    </li>
                    <li id="copy_style2">
                        <!--                        <a onclick="xadmin.add_tab('截图存档','upload_data.php')">-->
                        <!--                            <i class="iconfont">&#xe6a7;</i>-->
                        <!--                            <cite>截图存档</cite></a>-->
                        <a>
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>截图存档</cite></a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="iconfont left-nav-li" lay-tips="网络课数据">&#xe6da;</i>
                    <cite>网络课数据</cite>
                    <i class="iconfont nav_right">&#xe697;</i></a>
                <ul class="sub-menu">
                    <li>
                        <a onclick="xadmin.add_tab('导入数据','import_imooc.php')">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>导入数据</cite></a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="iconfont left-nav-li" lay-tips="关于系统">&#xe6b4;</i>
                    <cite>关于系统</cite>
                    <i class="iconfont nav_right">&#xe697;</i></a>
                <ul class="sub-menu">
                    <li>
                        <a onclick="xadmin.add_tab('开发团队','about.html')">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>开发团队</cite></a>
                    </li>
                    <li>
                        <a onclick="xadmin.add_tab('更新日志','log.html')">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>更新日志</cite></a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- <div class="x-slide_left"></div> -->
<!-- 左侧菜单结束 -->
<!-- 右侧主体开始 -->
<div class="page-content">
    <div class="layui-tab tab" lay-filter="xbs_tab" lay-allowclose="false">
        <ul class="layui-tab-title">
            <li class="home">
                <i class="layui-icon">&#xe68e;</i>系统首页
            </li>
        </ul>
        <div class="layui-unselect layui-form-select layui-form-selected" id="tab_right">
            <dl>
                <dd data-type="this">关闭当前</dd>
                <dd data-type="other">关闭其它</dd>
                <dd data-type="all">关闭全部</dd>
            </dl>
        </div>
        <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <iframe src='./welcome.php' frameborder="0" scrolling="yes" class="x-iframe"></iframe>
            </div>
        </div>
        <div id="tab_show"></div>
    </div>
</div>
<div class="page-content-bg"></div>
<style id="theme_style"></style>
<!-- 右侧主体结束 -->
<!-- 中部结束 -->
<script>//统计代码

</script>
<!--<div id="cc-myssl-id" style="position: fixed;right: 0;bottom: 0;width: 65px;height: 65px;z-index: 99;">-->
<!--    <a href="https://myssl.com/sju.idcs.vip?from=mysslid"><img src="https://static.myssl.com/res/images/myssl-id.png"-->
<!--                                                               alt="" style="width:100%;height:100%"></a>-->
<!--</div>-->
<script src="js/jquery.min.js"></script>
<script>
    $('#copy_style1,#copy_style2').click(function () {
        layui.use('layer', function () {
            var layer = layui.layer;
            layer.open({
                title: '提示消息'
                , content: '该模块正在开发中！'
            });

        });
    })
</script>
</body>

</html>