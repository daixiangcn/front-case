<!doctype html>
<?php
header("Content-type:text/html;charset=utf-8");
error_reporting(0);
$username = $_POST['username'];
// 取得客户端提交的密码并用md5()函数时行加密转换以便后面的验证
$password = md5($_POST['password']);
// 设置一个错误消息变量，以便判断是否有错误发生
// 以及在客户端显示错误消息。 其初值为空
$errmsg = '';
if (!empty($username)) { // 用户填写了数据才执行数据库操作
    // 数据验证, empty()函数判断变量内容是否为空
    if (empty($username)) {
        $errmsg = '数据输入不完整';
    }
    if (empty($errmsg)) { // $errmsg为空说明前面的验证通过
        // 调用mysqli的构造函数建立连接，同时选择使用数据库'test'
        $db = @new mysqli("cdb-g0344tqj.gz.tencentcdb.com:10124", "robot", "Azxcvbnm_2020", "sju_sys");
        // 检查数据库连接
        if (mysqli_connect_errno()) {
            $errmsg = "数据库连接失败!\n";
        } else {
            // 查询数据库，看用户名及密码是否正确
            $sql = "SELECT * FROM sys_user WHERE user_name='$username' AND user_password='$password'";
            $rs = $db->query($sql);
            // $rs->num_rows判断上面的执行结果是否含有记录，有记录说明登录成功
            if ($rs && $rs->num_rows > 0) {
                // 使用session保存当前用户
                session_start();
                $_SESSION['uid'] = $username;
                $errmsg = "登录成功!";
                // 更新用户登录信息
                $ip = $_SERVER['REMOTE_ADDR']; // 获取客户端的IP
                $sql = "UPDATE sys_user SET login_times = login_times + 1,";
                $sql .= "last_time=now(), login_ip='$ip' ";
                $sql .= " WHERE user_name='$username'";
                $db->query($sql);
                //重定向浏览器
                header("Location: index.php");
//确保重定向后，后续代码不会被执行
                exit;
            } else {
                $errmsg = "用户名或密码不正确，登录失败!";
            }
            // 关闭数据库连接
            $db->close();
        }
    }
}
?>

<html class="x-admin-sm">
<head>
    <meta charset="UTF-8">
    <title>三江学院团委辅助办公系统</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/login.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script src="./lib/layui/layui.js" charset="utf-8"></script>
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-bg">

<div class="login layui-anim layui-anim-up">
    <div class="message">三江学院团委辅助办公系统<sup>Beta</sup></div>
    <div id="darkbannerwrap"></div>

    <form method="post" class="layui-form" action="login.php">
        <input name="username" placeholder="用户名" type="text" lay-verify="required" class="layui-input"
               value="<? echo $username; ?>">
        <hr class="hr15">
        <input name="password" lay-verify="required" placeholder="密码" type="password" class="layui-input">
        <hr class="hr15">
        <p style="color: red"><? echo $errmsg; ?></p>
        <input value="登录" lay-submit lay-filter="login" style="width:100%;" type="submit">
        <hr class="hr20">
    </form>
</div>

<script>
    $(function () {
        layui.use('form', function () {
            var form = layui.form;
            // layer.msg('玩命卖萌中', function(){
            //关闭后的操作
            //   });
            //监听提交
            // form.on('submit(login)', function (data) {
            //     // alert(888)
            //     layer.msg(JSON.stringify(data.field), function () {
            //         location.href = 'index.html'
            //     });
            //     return false;
            // });
        });
    })
</script>
<!-- 底部结束 -->
<script>
    //百度统计可去掉
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>
</body>
</html>