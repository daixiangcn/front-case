<!DOCTYPE html>
<?php
session_start();
header("P3P: CP=CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR");
if (empty($_SESSION['uid'])) {
    echo "提示：您还没有登录，不能访问当前页面！<a href='login.php'>前往登录页面</a>";
    exit;
}
?>
<html class="x-admin-sm">
<head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.2</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">
                    数据上传
                </div>
                <div class="layui-card-body ">
                    <blockquote class="layui-elem-quote">
                        使用说明：直接将从官方平台上下载的Excel文件通过下面的文件上传模块上传至本系统，上传完成后系统会显示本次未参加学习的学生信息并记入未学习次数,如果出现问题，请联系系统管理员。
                    </blockquote>

                    <form action="excelUpload_imooc.php" enctype="multipart/form-data" method='post' class="layui-form">
                        <div class="layui-form-item">
                            <label class="layui-form-label">选择文件：</label>
                            <div class="layui-input-block">
                                <input type="file" name="file" accept=".xls" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="formDemo" type="submit" name="Submit"
                                        id="submit">
                                    立即提交
                                </button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="js/jquery.min.js"></script>
    <script>
        $('#submit').click(function () {
            layui.use('layer', function () {
                var layer = layui.layer;
                layer.open({
                    title: '提示信息'
                    , content: '数据正在上传至服务器,上传完成会自动跳转,请耐心等待！'
                });

            });
        })
    </script>
</body>
</html>