<!DOCTYPE html>
<?php
header("Content-type:text/html;charset=utf-8");
error_reporting(0);
$username = $_POST['username'];
// 取得客户端提交的密码并用md5()函数时行加密转换以便后面的验证
$password = md5($_POST['password']);
// 设置一个错误消息变量，以便判断是否有错误发生
// 以及在客户端显示错误消息。 其初值为空
$errmsg = 0;
if (!empty($username)) { // 用户填写了数据才执行数据库操作
    // 数据验证, empty()函数判断变量内容是否为空
    if (empty($username)) {
        $errmsg = '数据输入不完整';
    }
    if (empty($errmsg)) { // $errmsg为空说明前面的验证通过
        // 调用mysqli的构造函数建立连接，同时选择使用数据库'test'
        $db = @new mysqli("cdb-g0344tqj.gz.tencentcdb.com:10124", "robot", "Azxcvbnm_2020", "sju_sys");
        // 检查数据库连接
        if (mysqli_connect_errno()) {
            $errmsg = "数据库连接失败!\n";
        } else {
            // 查询数据库，看用户名及密码是否正确
            $sql = "SELECT * FROM sys_user WHERE user_name='$username' AND user_password='$password'";
            $rs = $db->query($sql);
// $rs->num_rows判断上面的执行结果是否含有记录，有记录说明登录成功
            if ($rs && $rs->num_rows > 0) {
// 使用session保存当前用户
                session_start();
                $_SESSION['uid'] = $username;
                $errmsg = "登录成功!";
// 更新用户登录信息
                $ip = $_SERVER['REMOTE_ADDR']; // 获取客户端的IP
                $sql = "UPDATE sys_user SET login_times = login_times + 1,";
                $sql .= "last_time=now(), login_ip='$ip' ";
                $sql .= " WHERE user_name='$username'";
                $db->query($sql);
//重定向浏览器
                header("Location: index.php");
//确保重定向后，后续代码不会被执行
                exit;
            } else {
                $errmsg = -1;
            }
// 关闭数据库连接
            $db->close();
        }
    }
}
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>共青团三江学院委员会智慧办公系统</title>
    <link rel="stylesheet" href="lib/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="css/login.css" media="all"/>
    <style>
        /* 覆盖原框架样式 */
        .layui-elem-quote {
            background-color: inherit !important;
        }

        .layui-input, .layui-select, .layui-textarea {
            background-color: inherit;
            padding-left: 30px;
        }
    </style>
    <!-- Jquery Js -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <!-- Layui Js -->
    <script type="text/javascript" src="lib/layui/layui.js"></script>
    <!-- Jqarticle Js -->
    <script type="text/javascript" src="assembly/jqarticle/jparticle.min.js"></script>
    <!-- ZylVerificationCode Js-->
    <script type="text/javascript" src="assembly/zylVerificationCode/zylVerificationCode.js"></script>
</head>
<body>
<!-- Head -->
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-sm12 layui-col-md12 zyl_mar_01">
            <blockquote class="layui-elem-quote" style="font-size: 25px; font-weight: bold;border-left: 10px solid #fff;"><img src="images/tuan_min.png" height="30px;">
                共青团三江学院委员会智慧办公系统
            </blockquote>
        </div>
    </div>
</div>
<!-- Head End -->

<!-- Carousel -->
<div class="layui-row">
    <div class="layui-col-sm12 layui-col-md12">
        <div class="layui-carousel zyl_login_height" id="zyllogin" lay-filter="zyllogin">
            <div carousel-item="">
                <div>
                    <img src="images/01.jpg" width="100%"/>
                </div>
                <div>
                    <img src="images/02.jpg" width="100%"/>
                </div>
                <div>
                    <img src="images/03.jpg" width="100%"/>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Carousel End -->

<!-- Footer -->
<div class="layui-row">
    <div class="layui-col-sm12 layui-col-md12 zyl_center zyl_mar_01">
        &copy; 2020 共青团三江学院委员会 版权所有
    </div>
</div>
<!-- Footer End -->


<!-- LoginForm -->
<div class="zyl_lofo_main">
    <fieldset class="layui-elem-field layui-field-title zyl_mar_02">
        <legend style="text-align: center">用户登录</legend>
    </fieldset>
    <div class="layui-row layui-col-space15">
        <form class="layui-form zyl_pad_01" method="post" action="login.php">
            <div class="layui-col-sm12 layui-col-md12">
                <div class="layui-form-item">
                    <input type="text" name="username" lay-verify="required|userName" autocomplete="on"
                           placeholder="账号" class="layui-input" value="<? echo $username; ?>">
                    <i class="layui-icon layui-icon-username zyl_lofo_icon"></i>
                </div>
            </div>
            <div class="layui-col-sm12 layui-col-md12">
                <div class="layui-form-item">
                    <input type="password" name="password" lay-verify="required|pass" autocomplete="off"
                           placeholder="密码"
                           class="layui-input">
                    <i class="layui-icon layui-icon-password zyl_lofo_icon"></i>
                </div>
            </div>
            <div class="layui-col-sm12 layui-col-md12">
                <div class="layui-row">
                    <div class="layui-col-xs4 layui-col-sm4 layui-col-md4">
                        <div class="layui-form-item">
                            <input type="text" name="vercode" id="vercode" lay-verify="required|vercodes"
                                   autocomplete="off" placeholder="验证码" class="layui-input" maxlength="4">
                            <i class="layui-icon layui-icon-vercode zyl_lofo_icon"></i>
                        </div>
                    </div>
                    <div class="layui-col-xs4 layui-col-sm4 layui-col-md4">
                        <div class="zyl_lofo_vercode zylVerCode" onclick="zylVerCode()"></div>
                    </div>
                </div>
            </div>
            <div class="layui-col-sm12 layui-col-md12">
                <br>
                <!--                <p>--><? // echo $errmsg; ?><!--</p>-->
            </div>
            <?php
            if ($errmsg != 0) {
                echo "<script>layui.use('layer', function(){var layer = layui.layer;layer.msg('用户名或密码错误，请重新输入！');});</script>";
            }
            ?>
            <div class="layui-col-sm12 layui-col-md12">
                <button class="layui-btn layui-btn-fluid" lay-submit="" style="background: #760002" type="submit">
                    立即登录
                </button>
            </div>
            <div class="layui-col-sm12 layui-col-md12">
                <br>
                <p>建议浏览器：IE10+;Chrome;Firefox </p>
                <p><i class="layui-icon layui-icon-download-circle "></i><a style="color: #760002" href="http://dl.mosisson.cn/71.0.3578.98_chrome32_stable_windows_installer.exe">下载Chrome for Windows</a> | <a style="color: #760002" href="http://dl.mosisson.cn/software/googlechrome%281%29.dmg">下载Chrome for MacOS</a> </p>
            </div>
        </form>
    </div>
</div>
<!-- LoginForm End -->
<script>
    layui.use(['carousel', 'form'], function () {
        var carousel = layui.carousel
            , form = layui.form;

        //自定义验证规则
        form.verify({
            username: function (value) {
                if (value.length < 2) {
                    return '账号至少得5个字符';
                }
            }
            , passwd: [/^[\S]{6,12}$/, '密码必须6到12位，且不能出现空格']
            , vercodes: function (value) {
                //获取验证码
                var zylVerCode = $(".zylVerCode").html();
                if (value != zylVerCode) {
                    return '验证码错误（区分大小写）';
                }
            }
            , content: function (value) {
                layedit.sync(editIndex);
            }
        });

        //监听提交
        form.on('submit(demo1)', function (data) {
            layer.alert(JSON.stringify(data.field), {
                title: '最终的提交信息'
            })
            return false;
        });


        //设置轮播主体高度
        var zyl_login_height = $(window).height() / 1.3;
        var zyl_car_height = $(".zyl_login_height").css("cssText", "height:" + zyl_login_height + "px!important");


        //Login轮播主体
        carousel.render({
            elem: '#zyllogin'//指向容器选择器
            , width: '100%' //设置容器宽度
            , height: 'zyl_car_height'
            , arrow: 'always' //始终显示箭头
            , anim: 'fade' //切换动画方式
            , autoplay: true //是否自动切换false true
            , arrow: 'hover' //切换箭头默认显示状态||不显示：none||悬停显示：hover||始终显示：always
            , indicator: 'none' //指示器位置||外部：outside||内部：inside||不显示：none
            , interval: '5000' //自动切换时间:单位：ms（毫秒）
        });

        //监听轮播--案例暂未使用
        carousel.on('change(zyllogin)', function (obj) {
            var loginCarousel = obj.index;
        });

        //粒子线条
        $(".zyl_login_cont").jParticle({
            background: "rgba(0,0,0,0)",//背景颜色
            color: "#fff",//粒子和连线的颜色
            particlesNumber: 100,//粒子数量
            //disableLinks:true,//禁止粒子间连线
            //disableMouse:true,//禁止粒子间连线(鼠标)
            particle: {
                minSize: 1,//最小粒子
                maxSize: 3,//最大粒子
                speed: 30,//粒子的动画速度
            }
        });

    });

</script>
</body>
</html>
