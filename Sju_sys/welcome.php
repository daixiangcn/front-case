<!DOCTYPE html>
<html class="x-admin-sm">
<?php
session_start();
header("P3P: CP=CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR");
if (empty($_SESSION['uid'])) {
    echo "提示：您还没有登录，不能访问当前页面！<a href='login.php'>前往登录页面</a>";
    exit;
}
?>
<head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.2</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php
require('db_config.php');
$sql = "select count(*) from `sys_user` ";
$result = $mysqli->query($sql);
$number = mysqli_fetch_row($result)[0];
$sql = "select search,file from `sys_data` ";
$result = $mysqli->query($sql);
while ($row = $result->fetch_row()) {
    $search = $row[0];
    $file_num = $row[1];
}
?>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body">
                    系统消息： “青年大学习”模块默认按院系进行匹配，导入数据后您可在搜索框输入班级号按班级导出Excel。<span style="color: red;font-weight: bold">注意：学号或班级号填写不完整的学生会被系统识别为“未完成学习”</span>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header"><i class="iconfont color"> &#xe70b;</i> 系统用户数</div>
                <div class="layui-card-body  ">
                    <p style="font-size: 20px;"> <?php echo $number ?> 人</p>
                    <p>各二级学院账户+管理员</p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header"><i class="iconfont color"> &#xe6f3;</i> 总运行时间</div>
                <div class="layui-card-body  ">
                    <p style="font-size: 20px;"> <?php echo floor((strtotime(date("Y-m-d H:i:s")) - strtotime("2020-02-24 00:00:00")) / 86400) * 24 + floor((strtotime(date("Y-m-d H:i:s")) - strtotime("2020-02-24 00:00:00")) % 86400 / 3600) ?>
                        小时</p>
                    <p>自2020年2月24日起</p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header"><i class="iconfont color"> &#xe806;</i> 用户查询数</div>
                <div class="layui-card-body  ">
                    <p style="font-size: 20px;"> <?php echo $search ?> 次</p>
                    <p>按查询次数计算</p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header"><i class="iconfont color"> &#xe74a;</i> 处理文件数</div>
                <div class="layui-card-body  ">
                    <p style="font-size: 20px;"> <?php echo $file_num ?> 份</p>
                    <p>按上传次数计算</p>
                </div>
            </div>
        </div>
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">常用模块</div>
                <div class="layui-card-body ">
                    <ul class="layui-row layui-col-space10 layui-this x-admin-carousel x-admin-backlog">
                        <li class="layui-col-md3 layui-col-xs6">
                            <a href="import_data.php" class="x-admin-backlog-body">
                                <h3>查询未学习的学生信息</h3>
                                <p>
                                    <cite><i class="iconfont search" style="font-size: 24px;"> &#xe73f;</i>
                                        青年大学习查询</cite>
                                </p>
                            </a>
                        </li>
                        <li class="layui-col-md3 layui-col-xs6">
                            <a href="import_imooc.php" class="x-admin-backlog-body">
                                <h3>查询未学习的学生信息</h3>
                                <p>
                                    <cite><i class="iconfont search" style="font-size: 24px;"> &#xe6da;</i>
                                        网络课数据查询</cite>
                                </p>
                            </a>
                        </li>
                        <li class="layui-col-md3 layui-col-xs6" id="copy_style1">
                            <a href="#" class="x-admin-backlog-body">
                                <h3>青年大学习统计数据</h3>
                                <p>
                                    <cite><i class="iconfont color" style="font-size: 24px;"> &#xe806;</i>
                                        统计数据查询</cite>
                                </p>
                            </a>
                        </li>
                        <li class="layui-col-md3 layui-col-xs6" id="copy_style2">
                            <a href="#" class="x-admin-backlog-body">
                                <h3>上传青年大学习截图</h3>
                                <p>
                                    <cite><i class="iconfont exl" style="font-size: 24px;"> &#xe802;</i>
                                        学习记录存档</cite>
                                </p>
                            </a>
                        </li>
                        <!--                        <li class="layui-col-md3 layui-col-xs6">-->
                        <!--                            <a href="download_music.php" class="x-admin-backlog-body">-->
                        <!--                                <h3>微信音视频资源</h3>-->
                        <!--                                <p>-->
                        <!--                                    <cite><i class="iconfont gifts" style="font-size: 24px;"> &#xe6db;</i> 音视频下载</cite>-->
                        <!--                                </p>-->
                        <!--                            </a>-->
                        <!--                        </li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--        <div class="layui-col-sm6 layui-col-md3">-->
    <!--            <div class="layui-card">-->
    <!--                <div class="layui-card-header">-->
    <!--                    <span class="layui-badge layui-bg-cyan layuiadmin-badge">月</span></div>-->
    <!--                <div class="layui-card-body  ">-->
    <!--                    <p class="layuiadmin-big-font">33,555</p>-->
    <!--                    <p>新下载-->
    <!--                        <span class="layuiadmin-span-color">10%-->
    <!--                                    <i class="layui-inline layui-icon layui-icon-face-smile-b"></i></span>-->
    <!--                    </p>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <div style=" background-color: #F2F2F2;">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-md6">
                <div class="layui-card">
                    <div class="layui-card-header">帮助信息</div>
                    <div class="layui-card-body ">
                        <div class="layui-collapse" lay-filter="test">
                            <div class="layui-colla-item">
                                <h2 class="layui-colla-title" style="color: red;font-weight: bold">【重要】“青年大学习”填写注意事项</h2>
                                <div class="layui-colla-content">
                                    <p>“青年大学习”填写信息时填写完整的班级号+姓名或者完整的学号+姓名，如：32017051A张三，32017051033李四。<span style="font-weight: bold;color: red">学号或班级号填写不完整的学生会被系统识别为“未完成学习”</span>。</p>
                                </div>
                            </div>
                            <div class="layui-colla-item">
                                <h2 class="layui-colla-title">“青年大学习”模块怎么使用，有没有具体的使用手册？</h2>
                                <div class="layui-colla-content">
                                    <p>1. 进入“青年大学习”模块会有具体的操作步骤及参考样表；<br>2. 点击下方按钮即可阅读/下载使用手册(由季凌尘老师编写)。</p>
                                    <button type="button" class="layui-btn"><a style="color: #fff;" href="files/download/manual(v1.0.200302).pdf"><i class="layui-icon layui-icon-file-b"></i>使用手册(v1.0.200302)</a></button>
                                </div>
                            </div>
                            <div class="layui-colla-item">
                                <h2 class="layui-colla-title">为什么“青年大学习”模块导出不了Excel数据？</h2>
                                <div class="layui-colla-content">
                                    <p>在本系统中，导出数据被安排在纯前端，导出数据的等待时间取决于查询的数据量及您电脑的处理性能。如果出现导出数据等待时间长（超过1分钟），可以通过右上角“联系技术支持工程师”按钮反馈您的情况。</p>
                                </div>
                            </div>
                            <div class="layui-colla-item">
                                <h2 class="layui-colla-title">如何向技术支持工程师反馈系统BUG?</h2>
                                <div class="layui-colla-content">
                                    <p>目前，您可以通过右上角“联系技术支持工程师”按钮反馈您的情况，需要注意的是，反馈情况时请携带错误截图及数据文件，便于工程师进行错误分析。</p>
                                </div>
                            </div>
                            <div class="layui-colla-item">
                                <h2 class="layui-colla-title">为什么点击“全选”框后导出的数据只有当前页的？</h2>
                                <div class="layui-colla-content">
                                    <p>这种情况在多页数据时出现，点击“全选”框的操作意味着“全选本页数据”，而不是所有页的数据。<br>如果您需要全选所有页的数据，请直接点击搜索框右侧的搜索按钮，然后再进行“全选”操作。</p>
                                </div>
                            </div>
                            <div class="layui-colla-item">
                                <h2 class="layui-colla-title">为什么导出的Excel学号列以科学计数法显示？</h2>
                                <div class="layui-colla-content">
                                    <p>出现此情况请将光标移动到A列右侧，然后双击，学号即可以文本形式显示。</p>
                                </div>
                            </div>
                            <div class="layui-colla-item">
                                <h2 class="layui-colla-title">为什么导出的Excel里的中文以乱码显示？</h2>
                                <div class="layui-colla-content">
                                    <p>该情况极少出现，出现此情况请通过右上角“联系技术支持工程师”按钮反馈您的情况，并将数据截图、数据源文件、系统导出文件作为附件一并发送。</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-col-md6">
                <div class="layui-card">
                    <div class="layui-card-header">系统信息</div>
                    <div class="layui-card-body ">
                        <table class="layui-table">
                            <tbody>
                            <tr>
                                <th>系统版本</th>
                                <td>1.0.200310</td>
                            </tr>
                            <tr>
                                <th>服务器地址</th>
                                <td><? echo $_SERVER['SERVER_NAME'] . '(' . $_SERVER['SERVER_ADDR'] . ')' ?></td>
                            </tr>
                            <tr>
                                <th>操作系统</th>
                                <td><? echo PHP_OS ?></td>
                            </tr>
                            <tr>
                                <th>运行环境</th>
                                <td>Nginx</td>
                            </tr>
                            <tr>
                                <th>PHP版本</th>
                                <td><? echo phpversion(); ?></td>
                            </tr>
                            <tr>
                                <th>PHP运行方式</th>
                                <td><? echo php_sapi_name(); ?></td>
                            </tr>
                            <tr>
                                <th>MYSQL版本</th>
                                <td>5.5.53</td>
                            </tr>
                            <tr>
                                <th>上传附件限制</th>
                                <td><? echo get_cfg_var("upload_max_filesize") ? get_cfg_var("upload_max_filesize") : "不允许" ?></td>
                            </tr>
                            <tr>
                                <th>执行时间限制</th>
                                <td><? echo get_cfg_var("max_execution_time") . "秒 " ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style id="welcome_style"></style>
</div>
</div>
</div>
<script src="js/jquery.min.js"></script>
<script>
    $('#copy_style1,#copy_style2').click(function () {
        layui.use('layer', function () {
            var layer = layui.layer;
            layer.open({
                title: '提示消息'
                , content: '该模块正在开发中！'
            });

        });
    })
</script>
</body>
</html>