<!DOCTYPE html>
<?php
session_start();
header("P3P: CP=CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR");
if (empty($_SESSION['uid'])) {
    echo "提示：您还没有登录，不能访问当前页面！<a href='login.php'>前往登录页面</a>";
    exit;
}
?>
<html class="x-admin-sm">
<head>
    <meta charset="UTF-8">
    <title>音频下载</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">
                    音视频下载
                </div>
                <div class="layui-card-body ">
                    <blockquote class="layui-elem-quote">
                        使用说明：直接将包含音频资源链接的微信公众号页面在微信中点击右上角按钮，然后点击“复制链接”，将复制的链接粘贴到下面的输入框中，系统会自动获取音频资源，获取完毕后会出现弹出框，点击弹出框右侧按钮即可下载。如果出现问题，请联系系统管理员。
                    </blockquote>

                    <form action='download_music.php' method='post' class="layui-form">
                        <div class="layui-form-item">
                            <label class="layui-form-label">完整的链接：</label>
                            <div class="layui-input-block">
                                <input type="text" name="str" required lay-verify="required"
                                       placeholder="请粘贴包含音频的微信公众号文章的链接" autocomplete="on" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="formDemo" type="submit">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </form>
                    <?php
                    error_reporting(0); // 不显示错误提示代码
                    $str = $_POST['str'];
                    if ($str != "") {
                        // 纯语音
                        if (strpos($str, 'voice_id') !== false) {
                            $str_1 = explode('&voice_id=', $str)[1];
                            $str_2 = explode('&_wxindex_=', $str_1)[0];
                            $str_3 = "https://res.wx.qq.com/voice/getvoice?mediaid=" . $str_2;
                            // echo $str_3;
                        } else {
                            // 非纯语音情况
                            $str_0 = file_get_contents($str);
                            if (strpos($str_0, 'voice_encode_fileid=') !== false) {
                                // 用户自己录制上传的音乐
                                $type = 0;
                                $str_1 = explode("voice_encode_fileid=\"", $str_0)[1];
                                $str_2 = explode("\" data-pluginname=", $str_1)[0];
                                $str_3 = "https://res.wx.qq.com/voice/getvoice?mediaid=" . $str_2;
                            } elseif (strpos($str_0, '<qqmusic') !== false) {
                                // 调取的QQ音乐
                                $type = 1;
                                $str_1 = explode("music_name=\"", $str_0)[1];
                                $str_2 = explode("\" singer=\"", $str_1)[0];
                                $str_3 = $str_2;
                            } elseif (strpos($str_0, ';vid=') !== false) {
                                // 来自腾讯视频的视频文件
                                $type = 2;
                                $str_1 = explode(";vid=", $str_0)[1];
                                $vid = explode("\">", $str_1)[0];
                                $api_url = "http://vv.video.qq.com/getinfo?vids=" . $vid . "&otype=json&charge=0&defaultfmt=shd";
                                $str_2 = file_get_contents($api_url);
                                $str_2_0 = explode("\"fn\":\"", $str_2)[1];
                                $fn = explode("\",\"", $str_2_0)[0];
                                $str_2_1 = explode("\"fvkey\":\"", $str_2)[1];
                                $vkey = explode("\",\"", $str_2_1)[0];
                                $str_3 = "http://vhotakamai.video.gtimg.com/" . $fn . "?vkey=" . $vkey;
                            }
                            // echo $str_2;
                        }
                        if ($type == 2) {
                            $html = "<script>layui.use('layer', function () {var layer = layui.layer;layer.open({type: 1,title: false ,closeBtn: false,area: '300px;',shade: 0.8,id: 'LAY_layuipro',btn: ['立即下载', '取消下载'],btnAlign: 'c',moveType: 1,content: '<div style=\"padding: 30px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;\">该视频由于传输协议限制，需要点击“立即下载”按钮前往下载页</div>',success: function(layero){var btn = layero.find('.layui-layer-btn');btn.find('.layui-layer-btn0').attr({href: '" . $str_3 . "',target: '_blank'});}});})</script>";
                        } elseif ($type == 1) {
                            $html = "<script>layui.use('layer', function () {var layer = layui.layer;layer.open({type: 0,title: '提示信息' ,content: '该音频来自QQ音乐，由于QQ音乐版权限制，不能下载。您可以通过以下方式在微信公众号中使用该音乐：点击“音频”，点击“音乐”，输入 “" . $str_3 . "” 即可插入该音频。' });})</script>";
                        } else {
                            $html = "<script>layui.use('layer', function () {var layer = layui.layer;layer.open({type: 2, title: '音视频资源已获取', content: ['" . $str_3 . "', 'no']});});</script>";
                        }
                        echo $html;
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

