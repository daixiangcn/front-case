<?php
header("Content-type:text/html;charset=utf-8");
session_start();
require('db_config.php');
if(isset($_SESSION["uid"]))  // 检测变量是否设置
{
    $sql = "UPDATE sys_user SET offline_time = now() WHERE user_name='".$_SESSION['uid']."'";
    $mysqli->query($sql);
    session_unset();  // 释放当前在内存中已经创建的所有$_SESSION变量，但是不删除session文件以及不释放对应的session id；
    session_destroy();  // 删除当前用户对应的session文件以及释放session id，内存中$_SESSION变量内容依然保留；
    echo $sql;
}
header("location:login.php"); // 重定向到登录界面
?>