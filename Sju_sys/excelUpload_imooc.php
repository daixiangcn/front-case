<!DOCTYPE html>
<html class="x-admin-sm">
<?php
session_start();
header("P3P: CP=CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR");
if (empty($_SESSION['uid'])) {
    echo "提示：您还没有登录，不能访问当前页面！<a href='login.php'>前往登录页面</a>";
    exit;
}
?>
<head>
    <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.2</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">
                    数据上传结果
                </div>
                <div class="layui-card-body ">
                    <?php
                    require('library/php-excel-reader/excel_reader2.php');
                    require('library/SpreadsheetReader.php');
                    require('db_config.php');
                    if (isset($_POST['Submit'])) {
                        $mimes = ['application/vnd.ms-excel','application/octet-stream', 'text/xls', 'application/vnd.oasis.opendocument.spreadsheet'];
//                        print_r($_FILES);
                        if (in_array($_FILES["file"]["type"], $mimes)) {
                            $uploadFilePath = 'files/upload/imooc/' . time() . basename($_FILES['file']['name']);
                            move_uploaded_file($_FILES['file']['tmp_name'], $uploadFilePath);
                            $class_name = preg_split("/\.[a-zA-z]{3}/",preg_split("/(\d+\-\d+)/",$_FILES['file']['name'])[1])[0];
//                            echo $class_name;
                            $Reader = new SpreadsheetReader($uploadFilePath);
                            // $totalSheet = count($Reader->sheets());
                            // echo "你有 ".$totalSheet." 张表".
                            $html = "<blockquote class=\"layui-elem-quote\">提示信息：已成功添加Excel中的数据至数据库,此次上传的数据如下表所示。点击按钮查询此次未学习名单 <button type=\"button\" class=\"layui-btn layui-btn-normal\"><a onclick=\"xadmin.open('".$class_name."未学习名单','show_imooc.php?class_name=".$class_name."')\" style='color: #fff'>查看此次未学习名单</a></button></blockquote><table class='layui-table' style='text-align: center'><thead><tr><th style='text-align: center'>姓名</th><th style='text-align: center'>学号</th><th style='text-align: center'>班级</th></tr></thead><tbody>";
                            // for($i=0;$i<$totalSheet;$i++){
                            // $Reader->ChangeSheet($i);
                            $Reader->ChangeSheet(0);
                            $i = 0;
                            $str = "";
                            foreach ($Reader as $Row) {
                                $html .= "<tr>";
                                $name = isset($Row[0]) ? $Row[0] : '';
                                $number = isset($Row[1]) ? $Row[1] : '';
                                $class = isset($Row[4]) ? $Row[4] : '';
                                if ($i == 0) {

                                } else {
                                    $html .= "<td>" . $name . "</td>";
                                    $html .= "<td>" . $number. "</td>";
                                    $html .= "<td>" . $class . "</td>";
                                    $html .= "</tr>";
                                    $str = $str . "('" . $name . "','" . $number . "','" . $class . "'),";
                                }
                                $i++;
                            }
                            $str = substr($str,0,strlen($str)-1);
                            $html .= "</tbody></table>";
                            echo $html;
                            $sql = "delete from imooc_temp";
                            $mysqli->query($sql);
                            $sql = "delete from imooc";
                            $mysqli->query($sql);
                            $sql = "insert into imooc_temp(name,number,class) values" . $str ;
//                            echo "<script>console.log($sql)</script>";
                            $mysqli->query($sql);
                            $sql = "update imooc_temp set name = replace(name,' ','') ";
                            $result = $mysqli->query($sql);
                            $sql = "insert into imooc select * from student where name not in ( select name from student where (student.name in (select name from imooc_temp) and student.class in (select class from imooc_temp) ) )";
                            $result = $mysqli->query($sql);
                        } else {
                            die("<script>layui.use('layer', function () {var layer = layui.layer;layer.open({type: 0, title: '提示信息', content: '对不起，您上传的不是符合要求的文件，仅允许上传后缀名为.xls的Excel文件,请将文件在Excel中另存为.xls文件！'});});</script>");
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>