<?php
header("content-type:text/html;charset=utf-8;");
require('db_config.php');
$random = intval($_GET['random']);
$sql = "select class from(select a.class,((b.total-count(*))/b.total) as real_proportion  from `" . $random . "` a join statical b on a.class = b.class and a.college = b.college group by a.class,b.total union select class,1 as real_proportion from statical where college = (select college from `" . $random . "` limit 1) and class not in (select class from `" . $random . "`))as new_list order by real_proportion desc";
$result_pie = $mysqli->query($sql);
$sql2 = "select real_proportion from(select a.class,((b.total-count(*))/b.total) as real_proportion  from `" . $random . "` a join statical b on a.class = b.class and a.college = b.college group by a.class,b.total union select class,1 as real_proportion from statical where college = (select college from `" . $random . "` limit 1) and class not in (select class from `" . $random . "`))as new_list order by real_proportion desc";
$result_pro = $mysqli->query($sql2);
//echo $sql;
$sql_college = "select college from $random limit 1";
$college = $mysqli->query($sql_college);
if($college!="本部"){
    $sql3 = "select * from(select count(*) as value,'未参与人数' as name  from `" . $random . "` union select (sum(total)-(select count(*) from `" . $random . "`)) as value,'已参与人数' as name from statical where campus = (select campus from `" . $random . "` limit 1)) as new_list";
}else {
  $sql3 = "select * from(select count(*) as value,'未参与人数' as name  from `" . $random . "` union select (sum(total)-(select count(*) from `" . $random . "`)) as value,'已参与人数' as name from statical where college = (select college from `" . $random . "` limit 1) and campus = (select campus from `" . $random . "` limit 1)) as new_list";  
}
$result_tot = $mysqli->query($sql3);
$sql4 = "select substr(time,6,8) as date from " . $random . "_temp group by date having date!= ''";
$result_dat = $mysqli->query($sql4);
$sql5 = "select number from(
select substr(time,6,8) as date,count(*) as number from " . $random . "_temp group by date having date!= '') as list";
$result_num = $mysqli->query($sql5);
$data_pie = array();
$proportion = array();
$total = array();
$date = array();
$number= array();
while ($row = mysqli_fetch_array($result_pie)) {  //MYSQL_ASSOC这个返回的数组是以数据表中的字段为键的而MYSQL_NUM是以数字为键的
    $data_pie[] = $row[0];
}
while ($row = mysqli_fetch_array($result_pro)) {  //MYSQL_ASSOC这个返回的数组是以数据表中的字段为键的而MYSQL_NUM是以数字为键的
    $proportion[] = $row[0];
}
while ($row = mysqli_fetch_array($result_tot)) {  //MYSQL_ASSOC这个返回的数组是以数据表中的字段为键的而MYSQL_NUM是以数字为键的
    $total[] = $row;
}
while ($row = mysqli_fetch_array($result_dat)) {  //MYSQL_ASSOC这个返回的数组是以数据表中的字段为键的而MYSQL_NUM是以数字为键的
    $date[] = $row[0];
}
while ($row = mysqli_fetch_array($result_num)) {  //MYSQL_ASSOC这个返回的数组是以数据表中的字段为键的而MYSQL_NUM是以数字为键的
    $number[] = $row[0];
}
$donation_data = array(
    'class' => $data_pie,
    'proportion' => $proportion,
    'total' => $total,
    'date'=> $date,
    'number'=> $number
);
echo json_encode($donation_data);
//echo $sql;
?>